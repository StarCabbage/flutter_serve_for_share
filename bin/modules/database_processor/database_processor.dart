import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:analyzer/dart/ast/ast.dart';
import 'package:dart_style/dart_style.dart';
import 'package:http/http.dart' as http;
import '../string_extension.dart';
import '../workers/string_worker.dart' show loadConfigFile, onAnswer;
import 'package:resource_portable/resource.dart' show Resource;
import '../utils.dart';

import 'src/functions_and_consts.dart';

String projectID;

Future<void> databaseGen() async {
  print('Вы инициализировали Firebase и gcp? Y/N');
  var answer = onAnswer(false, ['y', 'n']);
  if (answer == 'n') {
    print('Это необходимо');
    return;
  }
  print('А можно узнать путь к флаттеру? Если он у вас указан глобально, то писать ничего не нужно');
  var flutterPath = stdin.readLineSync();
  // var list = Directory.current.listSync();
  // var generateNewPackage = list
  //     .where((element) => element is Directory && element.path.contains('db_'))
  //     .isEmpty;
  if (!File('pubspec.yaml').existsSync()) {
    print('Понял. Выглядит так, что вы создаете новую базу данных. Я помогу Вам с этим.');

    print('Введите название проекта');
    var projectName = stdin.readLineSync();
    print('Вы уверены с данным названием ? $projectName y\\n');
    print('Если нет, то просто введите n и перезапустите снова программу');
    var answer = onAnswer(false, ['y', 'n']);
    if (answer == 'n') {
      return;
    } else {
      var libName = 'db_${projectName}';
      // lib dir
      await Process.runSync(flutterPath ?? 'flutter', ['create', '--template=package', libName], runInShell: true);

      var libDirectory = Directory('${libName}/lib');
      if (!File(libDirectory.path + '/${libName}.dart').existsSync()) {
        File(libDirectory.path + '/${libName}.dart').createSync(recursive: true);
      }
      File(libDirectory.path + '/${libName}.dart').writeAsStringSync('library ${libName};');

      // test dir
      var testDirectory = Directory('${libName}/test');
      if (!File(testDirectory.path + '/${libName}_test.dart').existsSync()) {
        File(testDirectory.path + '/${libName}_test.dart').createSync(recursive: true);
      }
      File(testDirectory.path + '/${libName}_test.dart').writeAsStringSync('void main() {}');

      // pubspec.yaml
      var pubspecString = 'name: ${libName}\n';
      pubspecString += """description: A new database package for ${projectName} project.
version: ${flutterServeVersion}
author:
homepage:

environment:
  sdk: ">=2.7.0 <3.0.0"

dependencies:
  firedart: ^0.8.3
  algolia: ^1.0.1
  eventsource:
    git: https://StarCabbage:07c8bd8aa8e79a620b6cfd5c227ff5923f243609@github.com/StarCabbage/dart-eventsource
  shelf: ^1.1.0
  googleapis_auth: any
  async: any
  mime: ^0.9.7
  merge_map: ^1.0.2
  event_container:
    git: https://StarCabbage:07c8bd8aa8e79a620b6cfd5c227ff5923f243609@github.com/StarCabbage/event_container

  
flutter_serve:
  miro_client_id: ''
  miro_client_secret: ''
  miro_board: ''
  root_name: ''
  project_id: ''""";
      File('${libName}' + '/pubspec.yaml').writeAsStringSync(pubspecString);
      File('${libName}' + '/.gcloudignore').writeAsStringSync('/.flutter_serve\n/.dart_tool');
      //
      // var cloudFuncDirectory =
      //     Directory('${libName}/.flutter_serve/cloud_functions_part');
      // File(cloudFuncDirectory.path + '/index.js').createSync(recursive: true);
      print('Создал тестовый пакет, заполни пожалуйста внутри раздел для flutter_serve.\n Не забудьте ещё добавить гугл сервис аккаунт');
      return;
    }
  }
  final args = loadConfigFile('pubspec.yaml', '');
  final String clientId = args['miro_client_id'] ?? '3074457351395217899';
  final String clientSecret = args['miro_client_secret'] ?? 'mWeHWdlaSFv60dWi8dGZApHgbphY4haQ';
  final String board = args['miro_board'] ?? '';
  final String rootName = args['root_name'] ?? 'root';
  projectID = args['project_id'];
  if (projectID == null) {
    throw 'no project_id in project pubspec.yaml';
  }
  var access_token = await miroAuthorize(clientId, clientSecret);

  var widgets = await getBoardWidgets(access_token['access_token'], board);
  var widgetsFile = File('./.flutter_serve/.miro_last_widgets_request.json');
  widgetsFile.createSync(recursive: true);
  widgetsFile.writeAsStringSync(jsonEncode(widgets));
  await generateDatabase();
  await Process.runSync(flutterPath ?? 'flutter', ['pub', 'get'], runInShell: true);
  return;
}

Future<Map> getBoardWidgets(String access_token, String board) async {
  var boardWidgetsRequest =
      Uri(scheme: 'https', host: 'api.miro.com', path: 'v1/boards/${board}/widgets', queryParameters: {'access_token': access_token});
  var boardWidgetsResponse = await http.get(boardWidgetsRequest);
  return jsonDecode(boardWidgetsResponse.body);
}

Future<Map> miroAuthorize(String clientId, String clientSecret) async {
  var miroTokenFile = File('.flutter_serve/.miro_token.json');
  var state = getRandomString(6);
  var miroMap = {};
  if (!miroTokenFile.existsSync() || miroTokenFile.readAsStringSync().isEmpty) {
    miroTokenFile.createSync(recursive: true);
    miroTokenFile.writeAsStringSync('{}');
  }
  miroMap = jsonDecode(miroTokenFile.readAsStringSync());
  if (miroMap.containsKey('access_token')) {
    return miroMap['access_token'];
  }
  if (miroMap.isEmpty || !miroMap.containsKey('code') || int.parse(miroMap['code_expire_timestamp']) < DateTime.now().millisecondsSinceEpoch) {
    var server = await HttpServer.bind(
      InternetAddress.loopbackIPv4,
      4040,
    );
    print('Waiting for miro auth on localhost:${server.port}');
    var redirectUri = 'http://localhost:${server.port}/miro';
    var miroAuthorize = Uri(
        scheme: 'https',
        host: 'miro.com',
        path: 'oauth/authorize',
        queryParameters: {'response_type': 'code', 'client_id': clientId, 'redirect_uri': redirectUri, 'state': state});
    runBrowser(miroAuthorize.toString());

    var request = await server.firstWhere((element) => element.method == 'GET' && element.uri.path.contains('miro'));

    var code = request.uri.queryParameters['code'];

    request.response
      ..statusCode = HttpStatus.accepted
      ..write('Now you can close this page');
    await request.response.close();

    miroMap['state'] = state;
    miroMap['code'] = code;
    miroMap['redirect_uri'] = redirectUri;
    miroMap['code_expire_timestamp'] = DateTime.now().add(Duration(minutes: 11)).millisecondsSinceEpoch.toString();

    miroTokenFile.writeAsStringSync(jsonEncode(miroMap));
    await server.close();
  }
  var miroAccessToken = Uri(scheme: 'https', host: 'api.miro.com', path: 'v1/oauth/token', queryParameters: {
    'grant_type': 'authorization_code',
    'client_id': clientId,
    'client_secret': clientSecret,
    'code': miroMap['code'],
    'redirect_uri': miroMap['redirect_uri']
  });
  var accessTokenResponse = await http.post(Uri.parse(miroAccessToken.toString()));
  miroMap['access_token'] = jsonDecode(accessTokenResponse.body);
  miroTokenFile.writeAsStringSync(jsonEncode(miroMap));
  return miroMap['access_token'];
}

var miroTexts = <String, MiroText>{};
var miroLines = <String, List<MiroLine>>{};
var miroLinesEnds = <String, List<MiroLine>>{};
Set<String> modelFilesNames = {};
List<String> customHandlerSwitchUrls = [];
List<String> generatedHandlerSwitchUrls = [];
List<String> customHandlerSwitchFuncs = [];
List<String> generatedHandlerSwitchFuncs = [];

Future<void> generateDatabase() async {
  // создаются 2 Map. В miroTexts по ключу ID находится текст
  // В miroLines по ключу startID находится endID и ID самой
  // линии. Эта линия связывает 2 виджета с этими ID ( startID и endID ).
  var widgetsFile = File('./.flutter_serve/.miro_last_widgets_request.json');

  var miroRowMap = jsonDecode(widgetsFile.readAsStringSync());
  for (var widget in (miroRowMap['data'] as List)) {
    if (widget['type'] == 'line') {
      // добавление в Map miroLines по startID
      if (miroLines[widget['startWidget']['id']] == null) {
        miroLines[widget['startWidget']['id']] = [];
      }
      miroLines[widget['startWidget']['id']].add(MiroLine(widget['id'], widget['endWidget']['id'], widget['style']['borderStyle']));

      // добавление в Map miroLinesEnds по endID
      if (miroLinesEnds[widget['endWidget']['id']] == null) {
        miroLinesEnds[widget['endWidget']['id']] = [];
      }
      miroLinesEnds[widget['endWidget']['id']].add(MiroLine(widget['id'], widget['startWidget']['id'], widget['style']['borderStyle']));
    } else {
      if (widget['type'] == 'text') {
        miroTexts[widget['id']] = MiroText(widget['text']);
      }
    }
  }

  var s1;
  s1 = Directory.current.toString();
  var s2;
  s2 = Directory.current.parent.toString();
  var libName = (s1.toString().substring(s2.toString().length).replaceAll("'", ''));

  // список функций копирования
  var copyFunctions = <String>[];

  // список case для db_handler
  var dbHandlerFunctions = <String>[];

  // список путей для index.js
  var urlPaths = <List<String>>[];
  var funcNames = <String>[];

  // нужно указать ID корня в rootID
  final args = loadConfigFile('pubspec.yaml', '');
  final String rootName = args['root_name'] ?? 'root';
  var rootID = miroTexts.entries.firstWhere((element) => element.value.text.contains(rootName)).key;
  miroTexts[rootID].text = 'root';
  var startID = rootID;
  Queue textQueue = Queue<String>();
  textQueue.add(startID);
  var enumSet = <String>{};
  enumSet.add(stringCleaner(miroTexts[startID].text).toLowerCase().toCamelCase());
  while (textQueue.isNotEmpty) {
    String currentID = textQueue.first;
    textQueue.removeFirst();
    if ((miroLines[currentID] != null)) {
      for (var lineToChild in miroLines[currentID]) {
        if (stringCleaner(miroTexts[lineToChild.endId].text).startsWith('_')) {
          for (var connectingLine in miroLines[lineToChild.endId]) {
            enumSet.add(stringCleaner(miroTexts[connectingLine.endId].text));
          }
          continue;
        }
        if (!(miroTexts[lineToChild.endId].text).contains('|') && !((lineToChild.borderStyle == 'dotted'))) {
          if (stringCleaner(miroTexts[lineToChild.endId].text).split(':').first.isNotEmpty) {
            enumSet.add(stringCleaner(miroTexts[lineToChild.endId].text.split(':').first).toLowerCase().toCamelCase());
          }
        }
        if (lineToChild.borderStyle == 'normal') {
          textQueue.add(lineToChild.endId);
        }
        if (lineToChild.borderStyle == 'dotted') {
          // создание функций для index.js
          var funcdef = miroTexts[lineToChild.endId].text;
          var currentBranchID = currentID;
          //Определение названия функции
          var funcName = stringCleaner(funcdef.replaceAll('#onChange:', ''));
          var stringFuncPath = funcName.split('#')[0].replaceAll(' ', '');
          funcName = funcName.substring(funcName.indexOf('#'));
          funcName = funcName.split('#')[1];

          // создание функции копирования
          if (textToIndexSolverFunction(
              currentBranchID: currentBranchID, funcdef: funcdef, flutterServeDir: flutterServeDir, urlPaths: urlPaths, funcNames: funcNames)) {
            // место, куда будет происходить копирование
            // print('miroLines[lineToChild.endId]: ${miroLines[lineToChild.endId]} ${lineToChild.endId}');
            var targetPathId = miroLinesEnds[lineToChild.endId].first.endId;
            var backPath = reversePath(targetPathId);

            copyFunctions.add(copyFunc(backPath, funcName));

            // создание case для db_handler
            dbHandlerFunctions.add(dbHandlerSwitchCaseMaker(stringPath: stringFuncPath, funcName: funcName));
          }
        }
      }
      if (flagCreateModelFile(currentID)) {
        modelFileGenerator(currentID, modelsDirPath: modelsDirPath, libName: libName);
      }
      if (flagCreateMapModelFile(currentID)) {
        var variableType = findMapFileType(currentID: currentID);
        print('(${miroTexts[currentID].text})${variableType}');
        var classType = stringCleaner(miroTexts[currentID].text);
        // if (miroTexts[miroLines[currentID].first.endId].text.contains('<u>')) {
        //   variableType += '_map';
        // }
        print('!${variableType}');
        mapModelFileMaker(classType: mapFileNameCleaner(classType), variableType: variableType, libName: libName);
      }
      if (flagCreateListModelFile(currentID)) {
        var variableType = findMapFileType(currentID: currentID);
        print('(${miroTexts[currentID].text})${variableType}');
        var listModelName = stringCleaner(miroTexts[currentID].text);
        print('!${variableType}');
        listModelFileMaker(className: listModelName, variableType: variableType, libName: libName);
      }
    }
  }

  createFiles(
      copyFunctions: copyFunctions,
      libName: libName,
      urlPaths: urlPaths,
      funcNames: funcNames,
      projectID: projectID,
      dbHandlerFunctions: dbHandlerFunctions,
      modelFilesNames: modelFilesNames,
      enumSet: enumSet);

  // проверка README.md на содержание строки
  var generatedReadmeFile = readmeFileGenerator(libName: libName, projectID: projectID);
  File('README.md').writeAsStringSync(generatedReadmeFile);
}
