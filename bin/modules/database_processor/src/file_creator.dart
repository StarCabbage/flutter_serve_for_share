// создание файла
import 'dart:io';

import 'package:dart_style/dart_style.dart';
import '../database_processor.dart';
import 'functions_and_consts.dart';

void createFileFromString(Directory dirPath, String fileName, String s, {String fileType = '.dart', bool isNeedFormat = false}) {
  var workPath = dirPath.path + (dirPath.path.isNotEmpty ? '/' : '') + '${fileName}${fileType}';
  if (!File(workPath).existsSync()) {
    File(workPath).createSync(recursive: true);
  }
  File(workPath).writeAsStringSync(isNeedFormat ? DartFormatter().format(s) : s);
  // ..writeAsStringSync(s);
}

void createExtendedFile(String mainClassID, String extendingClassID, {Directory modelsDirPath, String libName}) {
  var importsSet = <String>{};
  var id = extendingClassID;
  var fileStrings = <String>[];
  fileStrings.add("import 'native_types/types.dart';");
  fileStrings.add("import '../../${libName}.dart';");
  fileStrings.add('');
  //class
  var tmpString = '';
  tmpString = 'class ';

  var className = stringCleaner(miroTexts[id].text);
  tmpString += className.toCamelCase().capitalizeFirst();
  tmpString += ' extends ';
  var extendingType = stringCleaner(miroTexts[mainClassID].text).capitalizeFirst();

  var isMapClass = false;
  if (miroTexts[id].text.contains('<u>')) {
    isMapClass = true;
    var valType = '';
    var typeAndVar = stringCleaner(miroTexts[miroLines[id].first.endId].text).split('|');
    var variableNameAndType = typeAndVar[1].split(':');
    if (variableNameAndType.length == 2) {
      valType = variableNameAndType.last;
    } else {
      valType = 'String';
    }
    var isStandardType = false;
    valType = valType.toCamelCase();
    valType = standardTypeDefiner(varType: valType, boolVar: isStandardType);
    // print("@7${valType}");
    extendingType = 'DbMap<DbString, ${valType}>';
  }
  tmpString += extendingType.toCamelCase().capitalizeFirst();
  tmpString += '{\n';
  tmpString += '${className.toCamelCase().capitalizeFirst()}([String key]) : super(key);';
  fileStrings.add(tmpString);

  fileStrings.add('');
  bool isStandartType;
  for (var textWidget in miroLines[id]) {
    if (textWidget.borderStyle == 'dashed') {
      createExtendedFile(id, textWidget.endId, modelsDirPath: modelsDirPath, libName: libName);
    }
    var currentTextWidget = stringCleaner(miroTexts[textWidget.endId].text);
    var childMap = '';
    if (miroLines[textWidget.endId] != null) {
      if (isMapClass) {
        continue;
      }
      childMap = stringCleaner(miroTexts[miroLines[textWidget.endId].first.endId].text);
    }

    if ((miroLines[textWidget.endId] != null) && ((childMap.split('|')).length == 2)) {
      // List keyNameAndType = (childMap.split('|').first).split(':');
      var keyNameAndType = (childMap.split('|').first.split(':'));
      var keyType = 'DbString';
      // if (keyNameAndType.length == 2) {
      //   keyType = keyNameAndType.last;
      // }

      isStandartType = true;
      if (keyNameAndType.length == 2) {
        keyType = standardTypeDefiner(varType: keyNameAndType.last, boolVar: isStandartType);
        print("#1${keyNameAndType}");

        if (isStandartType != true) {
          keyType = keyNameAndType.last;
        }
      }

      var valNameAndType = (childMap.split('|').last).split(':');

      var varType = 'DbString';
      isStandartType = true;
      if (valNameAndType.length == 2) {
        varType = standardTypeDefiner(varType: valNameAndType.last, boolVar: isStandartType);
        print("#2${valNameAndType}");

        if (isStandartType != true) {
          varType = valNameAndType.last;
        }
      }

      tmpString = makeGetterMap(keyType, varType, currentTextWidget, libName: libName, currentID: textWidget.endId);
      fileStrings.add(tmpString);

      tmpString = makeSetter(currentTextWidget);
      fileStrings.add(tmpString);
      fileStrings.add('');
    } else {
      if (miroLines[textWidget.endId] != null) {
        tmpString =
            makeGetterMap('DbString', (childMap.split('|').last).split(':').last, currentTextWidget, libName: libName, currentID: textWidget.endId);
        fileStrings.add(tmpString);

        tmpString = makeSetter(currentTextWidget);
        fileStrings.add(tmpString);

        fileStrings.add('');

        var importClassString = makeImportModel((childMap.split('|').last).split(':').last);
        if (!importsSet.contains(importClassString)) {
          fileStrings.insert(1, importClassString);
          importsSet.add(importClassString);
        }
        fileStrings.insert(1, importClassString);
      } else {
        var nameAndType = currentTextWidget.split(':');
        var varType = 'DbString';
        isStandartType = true;
        if (nameAndType.length == 2) {
          varType = standardTypeDefiner(varType: stringCleaner(nameAndType.last), boolVar: isStandartType);
        }
        print("#3${nameAndType}");

        tmpString = makeGetterVar(varType, nameAndType.first);
        fileStrings.add(tmpString);

        tmpString = makeSetter(stringCleaner(nameAndType.first));
        fileStrings.add(tmpString);

        fileStrings.add('');
      }
    }
  }
  fileStrings.add('@override\n');
  fileStrings.add(
      'Future<${className.toCamelCase().capitalizeFirst()}> get future async => await super.future as ${className.toCamelCase().capitalizeFirst()};');
  fileStrings.add('@override\n');
  fileStrings
      .add('Stream<${className.toCamelCase().capitalizeFirst()}> get stream => super.getStream<${className.toCamelCase().capitalizeFirst()}>();');
  fileStrings.add('@override\n');
  fileStrings
      .add('Stream<${className.toCamelCase().capitalizeFirst()}> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);');
  fileStrings.add("""
    @override
  ${className.toCamelCase().capitalizeFirst()} get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<${className.toCamelCase().capitalizeFirst()}> set(value) async {
    if (value is ${className.toCamelCase().capitalizeFirst()}) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    """);

  // fileStrings.add('@override\n');
  // fileStrings.add(
  //     '${className.toCamelCase().capitalizeFirst()} get value => super.value;');

  fileStrings.add('}');
  var resultString = '';
  for (var s in fileStrings) {
    resultString += s;
    resultString += '\n';
  }

  modelFilesNames.add((className.toLowerCase()) + '_model');
  createFileFromString(modelsDirPath, (className.toLowerCase()) + '_model', resultString);
}
