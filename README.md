# Flutter Serve

A command-line tool which simplifies the task of working with images and strings(for now).

## :sparkles: What's New

#### Version 0.0.1 (18th Apr 2020)

- Worked on --files and --strings commands

## :book: Guide

#### 1. Setup the config file

flutter pub global activate --source git https://StarCabbage@github.com/StarCabbage/flutter_serve

запускать нужно внутри папки со всеми проектами

flutter pub global run flutter_serve

Версию flutter_serve нужно менять в файле functions_and_consts.dart

Для того, чтобы записать в БД 1 достаточно использовать такую запись: (db.root.value = 'str')

Для активации flutter_serve можно написать - flutter pub run flutter_serve --LaunchFlags_files

Для работы с языками:

```
flutter_serve:
  appLocalizationsLink: "import '../../utils/app_localizations.dart' show AppLocalizations;"
  languageFile: "langs/ru_RU.json"
  languageFolder: "langs"
```

Не забудь добавить:

```
flutter_localizations:
    sdk: flutter
flutter_cupertino_localizations: ^1.0.1
```

А также не забудь добавить внутрь класса MaterialApp:

```
localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
supportedLocales: AppLocalizations.supportedLocales,
```

Ну и в начале main пропишите:

```
AppLocalizations.supportedLocales.add(Locale('en','US'));
```
